import * as React from 'react';
import './App.css';

class App extends React.Component {
  public state = {
    id: 1,
    key: 'Key'
  }
  public clickHandler = (e: any) => {
    this.setState({
      key: 'Key2'
    });
  }

  public userTypeHandler = (e: any) => {
    this.setState({
      key: e.target.value
    });
  }

  public render() {
    return (
      <div className="App">
        <header>
          <h1>Welcome</h1>
        </header>
        <form>
          <input type='text' onChange={this.userTypeHandler} />
          <button>Submit</button>
        </form>
        <p>
          {this.state.id} 
          {this.state.key} 
          <button onClick={this.clickHandler}>Click here</button>
        </p>
      </div>
    );
  }
}

export default App;
