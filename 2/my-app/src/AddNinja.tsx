import * as React from 'react';

interface IAddNinjaState {
    name: string | null;
    age: number | null;
    belt: string | null;
}

class IAddNinja extends React.Component<any, IAddNinjaState> {
    public state = {
        name: null,
        age: null,
        belt: null
    }
    public handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.id === 'name') {
            this.setState({
                name: e.target.value
            })
        } else if (e.target.id === 'age') {
            this.setState({
                age: parseInt(e.target.value, 10)
            })
        } else if (e.target.id === 'belt') {
            this.setState({
                belt: e.target.value
            })
        }
    }
    public handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.addNinja(this.state);
    }
    public render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="name">Name:</label>
                    <input type="text" id="name" onChange={this.handleChange} />
                    <label htmlFor="name">Age:</label>
                    <input type="text" id="age" onChange={this.handleChange} />
                    <label htmlFor="name">Belt:</label>
                    <input type="text" id="belt" onChange={this.handleChange} />
                    <button>Submit</button>
                </form>
            </div>
        );
    }
}

export default (IAddNinja);