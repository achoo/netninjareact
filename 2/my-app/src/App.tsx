import * as React from 'react';
import Ninjas from './Ninjas';
import AddNinja from './AddNinja';

class App extends React.Component {
  public state = {
    ninjas: [
      { name: "Someone", age: 30, belt: "black", id: 1 },
      { name: "Something", age: 31, belt: "white", id: 2 },
      { name: "Sometime", age: 32, belt: "yellow", id: 3 },
    ]
  }
  public addNinja = (ninjaInput: {
    name: string;
    age: number;
    belt: string;
  }) => {
    const ninja = {
      ...ninjaInput,
      id: Math.random()
    };
    this.setState({
      ninjas: this.state.ninjas.concat([ninja])
    })
  }

  public deleteNinja = (id: number) => {
      const deletedNinjaArr = this.state.ninjas.filter(ninja => {
          return ninja.id !== id
      })
      this.setState({
        ninjas: deletedNinjaArr
      })
  }

  public render() {
    return (
      <div className="App">
        <h1>Header</h1>
        <p className="App-intro">Welcome</p>
        <Ninjas ninjas={this.state.ninjas} context={this} deleteNinja={this.deleteNinja} />
        <AddNinja addNinja={this.addNinja} />
      </div>
    );
  }
}

export default App;