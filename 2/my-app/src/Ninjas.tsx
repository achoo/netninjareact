import * as React from 'react';

interface INinjasProps {
    ninjas: Array<{
        name: string,
        age: number,
        belt: string,
        id: number
    }>,
    context: React.Component,
    deleteNinja: (id:number)=>void
}


const Ninjas = (props: INinjasProps) => {
    const { ninjas, deleteNinja, context } = props;
    const ninjaList = ninjas.map(ninja => {
        return (
            <div key={ninja.id} className="ninja">
                <div>Name: {ninja.name}</div>
                <div>Age: {ninja.age}</div>
                <div>Belt: {ninja.belt}</div>
                <button onClick={deleteNinja.bind(context, ninja.id)}>Delete</button>
            </div>
        )
    })
    return (
        <div className="ninja-list">
            {ninjaList}
        </div>
    )
}

export default Ninjas;