import * as React from 'react';

interface IAppState{
    id: number,
    content: string
  }

interface IAddTodo {
    addTodo: (id:{})=>void,
}

class AddTodo extends React.Component<IAddTodo,IAppState> {
    public state = {
        id: 1,
        content: ''
    }
    public handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            content: e.target.value
        })
    }
    public handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.addTodo(this.state);
        this.setState({
            content: ""
        })
    }
    public render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <label>Add new todo:</label>
                    <input type="text" onChange={this.handleChange} value={this.state.content} />
                </form>
            </div>
        )
    }
}

export default AddTodo;