import * as React from 'react';
import Todos from './todos';
import AddTodo from './AddForm';

interface ITodo {
  id: number,
  content: string
}

interface IAppState{
  todos: ITodo[]
}

class App extends React.Component<{},IAppState>{
  public state = {
    todos: [
      {
        id: 1,
        content: "Jump"
      },
      {
        id: 2,
        content: "Sit"
      }
    ]
  }

  public deleteTodo = (id: number) => {
    const todos = this.state.todos.filter(todo => {
      return todo.id !== id
    });
    this.setState({
      todos,
    }) 
  }

  public addTodo = (todoOutput: {
    id: number,
    content: string
  }) => {
    const todo = {
      ...todoOutput,
      id: Math.random()
    };
    this.setState({
      todos: this.state.todos.concat([todo])
    })
  }
   
  public render() {
    return (
      <div className="todo-app container">
        <h1 className="center">Todo's</h1>
        <Todos todos={this.state.todos} deleteTodo={this.deleteTodo} context={this}  />
        <AddTodo addTodo={this.addTodo} />
      </div>
    );
  }
}

export default App;
