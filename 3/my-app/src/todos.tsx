import * as React from 'react';

interface ITodo {
    id: number,
    content: string,
}

interface ITodosProps{
    todos: ITodo[],
    deleteTodo: (id:number)=>void,
    context: React.Component,
} 

const Todos = (props: ITodosProps) => {
    const todoList = props.todos.length  ? (
        props.todos.map(todo => {
            return (
                <div className="collection-item" key={todo.id}>
                    <span onClick={props.deleteTodo.bind(props.context, todo.id)}>{todo.content}</span>
                </div>
            )
        })
    ) : (
            <p className="center">You have finished your todo list</p>
        )
    return (
        <div className="todos collection">
            { todoList }
        </div>
    )
}

export default Todos;