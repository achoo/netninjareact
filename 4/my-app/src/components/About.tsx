import * as React from 'react';

const About = () => {
    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, dicta aliquid vel nobis maiores laboriosam quae dolor harum reiciendis aut obcaecati dolore, totam esse tempora assumenda porro veniam! Consequuntur, sint!</p>
        </div>
    )
}

export default About;