import * as React from 'react';

const Contact = () => {
    return (
        <div className="container">
            <h4 className="center">Contact</h4>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, dicta aliquid vel nobis maiores laboriosam quae dolor harum reiciendis aut obcaecati dolore, totam esse tempora assumenda porro veniam! Consequuntur, sint!</p>
        </div>
    )
}

export default Contact;