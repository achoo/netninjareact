import * as React from 'react';
import { Link } from 'react-router-dom';
import Pokeball from '../pokeball.png';
import { connect } from 'react-redux';
import { IRootState } from 'src/reducers/rootReducer';

interface IPropsHome {
    posts: Array<{
        id: string,
        title: string,
        body:string
    }>
}

class Home extends React.Component<IPropsHome> {

    public render() {
        const { posts } = this.props;
        const postList = posts.length ? (
            posts.map(post => {
                return (
                    <div className="post card" key={post.id}>
                        <img src={Pokeball} alt="A pokeball"/>
                        <div className="card-content">
                            <Link to={'/' + post.id} >
                                <span className="card-title red-text">{post.title}</span>
                            </Link>
                        </div>
                    </div>
                )
            })
        ) : (
                <div className="center">No posts yet</div>
            )
        return (
            <div className="container home">
                <h4 className="center">Home</h4>
                { postList }
            </div>
        )
    }
}

const mapStateToProps = (state: IRootState) => {
    return {
        posts: state.posts
    }
}

export default connect(mapStateToProps)(Home);