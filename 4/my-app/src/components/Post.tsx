import * as React from 'react';
import { connect } from 'react-redux';
import { IRootState } from 'src/reducers/rootReducer';
import { match } from 'react-router';

export interface IPostOwnProps {
    // generic
    match: match<{
        post_id: string
    }>
}

export interface IPostProps extends IPostOwnProps {
    post?: {
        id: string,
        title: string,
        body: string,
    },
}

class Post extends React.Component<IPostProps, {}> {
    
    handleClick = () => {
        this.props.deletePost(this.props.post.id);
        this.props.history.push('/');
    }

    public render() {
        const post = this.props.post ? (
            <div className="post">
                <h4 className="center">{this.props.post.title}</h4>
                <p>{this.props.post.body}</p>
                <div className="center">
                <button className="btn grey" onclick={this.handleClick}>
                    Delete Post
                </button>
                </div>
            </div>
        ) : (
                <div className="center">Loading post...</div>
            )
        return (
            <div className="container">
                {post}
            </div>
        );
    }
}

const mapStateToProps = (state: IRootState, ownProps: IPostOwnProps) => {
    const id = ownProps.match.params.post_id;
    return {
        post: state.posts.find((post: any) => {
            return post.id === id
        })
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        deletePost: (id: any) => { dispatch({type: 'DELETE_POST', id: id}) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Post)